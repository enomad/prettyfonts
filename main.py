# -*- coding: utf-8 -*-


from pyparsing import *

from pyparsing import OneOrMore

def parse_shp(shp_string):

    enclosed = Forward()
    nestedParens = (nestedExpr('(', ')', content=enclosed) )

    yo = Word(alphanums  + '-') 

    enclosed << (yo | ',' | ' ' | nestedParens )

    enf = OneOrMore(enclosed)

    enf.ignore(',')

    # data = '(gimme (some (nested, nested (lists))))' 
    # data = 'gimme' 

    # word_of_everything_except_a_comma = Word(''.join(c for c in printables if c != ','))


    # data = "2 , 14,8, ( -4,-25),8,(2,24),1,01A,016,012,01E,02C,02B,01A,2,8,(8,5),\
    # 1,01A,016,012,01E,02C,02B,01A,2,8,(9,-19),14,8,(-11,9),0"


    data = """*00047,53,ucg
    2,14,3,2,14,8,(-15,-42),14,4,2,8,(15,16),1,025,026,027,048,029,
    02A,02B,8,(-1,-3),05C,8,(1,-3),02D,02E,02F,040,021,022,023,034,
    058,2,8,(11,-8),14,3,2,14,8,(-27,-21),14,4,2,0

    *00048,30,uch
    2,14,8,(-7,-21),1,8,(0,21),2,0E0,1,8,(0,-21),2,8,(-14,11),1,0E0,
    2,8,(8,-11),14,8,(-15,-10),0

    *00049,18,uci
    2,14,8,(0,-21),1,8,(0,21),2,8,(8,-21),14,8,(-8,-10),0

    *0004A,33,ucj
    2,14,8,(-5,-21),8,(10,21),1,8,(0,-16),8,(-1,-3),01A,029,028,027,
    016,8,(-1,3),024,2,8,(16,-7),14,8,(-11,-10),0
    """

    fileobj = open('test_font.shp', 'rb')

    data = fileobj.read()

    header = Word("*") \
         + Word(alphanums).addParseAction(lambda t: ('code', t[0])) \
         + Word(",") + Word(nums) + Word(",") + Word(printables+'�')


    # header.ignore(',')


    entry = header.addParseAction(lambda t: ('code', t[1][1])) + enf.addParseAction(lambda t: ('path', t)) 


    entry.addParseAction(lambda t: [t]) 

    entries = OneOrMore(entry)

    # shp_string = data 
    data = entries.parseString(data).asList()

    ll = {}

    for k,v in data:
        ll[int(k[1], 16)] = v[1]

    return ll

    # import IPython; IPython.embed()

    # return entry.parseString(data).asList()


paths = parse_shp('')
# 0/0

# import IPython; IPython.embed()



# attribute = (Optional('!') + Word(alphas + nums + '_') + Literal(":") + Word(alphas + nums + '_')).setParseAction(Attribute.parse) 

# Values = Value + Maybe(',')

# Skobki = '(' + Values + ')'

# All = OneOrMore(Value | Skobki )

# data = '!sdf:dfsg sdf:df !sdfsdg:sdf' 

# print enclosed.parseString(data).asList()
# print enclosed.parseString(data)


# def parse(data):
    # return All.parseString(data)    
# import IPython; IPython.embed()



# data = "2,14,8,"

# value = Word(alphanums) + Optional(',')

# greet = OneOrMore(value)
# greeting = greet.parseString( "2,14,8,(1,2)" )

# print greeting


t = """
2,14,8,(-5,-21),8,(10,21),1,8,(0,-16),8,(-1,-3),01A,029,028,027,
016,8,(-1,3),024,2,8,(16,-7),14,8,(-11,-10),0
"""

# parse_shp(t)

# stream = iter(parse_shp(t))

def to_int(val):
    if val.startswith('0'):
        return int(val, 16)
    else:
        return int(val)


def naprav(direction, h):
    # http://docs.autodesk.com/ACD/2010/ENU/AutoCAD%202010%20User%20Documentation/index.html?url=WS73099cc142f4875513fb5cd10c4aa30d6b-7ec0.htm,topicNumber=d0e406895
    if direction == 0:
        return (h, 0)
    
    elif direction == 1:
        return (h, h/2)

    elif direction == 2:
        return (h, h)

    elif direction == 3:
        return (h/2, h)

    elif direction == 4:
        return (0, h)


    elif direction == 5:
        return (-h/2, h)

    elif direction == 6:
        return (-h, h)

    elif direction == 7:
        return (-h, h/2)

    elif direction == 8:
        return (-h, 0)

    elif direction == 9:
        return (-h, -h/2)

    elif direction == 10:
        return (-h, -h)


    elif direction == 11:
        return (-h/2, -h)

    elif direction == 12:
        return (0, -h)

    elif direction == 13:
        return (h/2, -h)
    
    elif direction == 14:
        return (h, -h)

    elif direction == 15:
        return (h, -h/2)


    raise Exception('cant %s %s',direction, h )

def run(ctx, stream, initial_scale):

    draw_on = False

    scale = 1.0 / initial_scale

    def move_or_draw(x ,y):

        if draw_on:
            ctx.rel_line_to(x*scale, y*scale)
        else:
            ctx.rel_move_to(x*scale, y*scale)
        
    is_vertical = False

    while True:
        cmd = stream.next()

        # print cmd

        # if is_vertical:
        #     is_vertical = False
        #     continue

        
        if len(cmd)>1 and cmd.startswith('0'):
            # print cmd
            length = int(cmd[1], 16)
            direction = int(cmd[2], 16)
            print "draw", [direction, length]

            move_or_draw(*naprav(direction, length))
        
        else:
                
            cmd = to_int(cmd)
            if cmd == 0:
                print 'end'
                return 
            elif cmd == 2:
                print 'pen up'
                draw_on = False

            elif cmd == 14:
                print 'process only if vertical'
                is_vertical = True

            elif cmd == 8:
                [x, y] = stream.next()
                print 'X-Y displacement given by next two bytes: ', x, " ", y
                if is_vertical:
                    is_vertical = False
                    continue
                move_or_draw(float(x), float(y) )

            elif cmd== 9:
                print "Multiple X-Y displacements, terminated (0,0)"

                
                while True:        
                    [x, y] = stream.next()
                    x = to_int(x)
                    y = to_int(y)

                    print [x, y]
                    if x == 0 and y == 0:
                        break

                    # if is_vertical:
                    move_or_draw(float(x), float(y) )

            # if is_vertical:
            #         is_vertical = False

                
            elif cmd == 1:
                print 'Activate Draw mode (pen down)'
                draw_on = True
                
            elif cmd == 3:
                multiply_scale = int(stream.next())
                if is_vertical:
                    is_vertical = False
                    continue
                print 'Divide vector lengths by next byte ', multiply_scale
                scale *= multiply_scale
            
            elif cmd == 4:
                if is_vertical:
                    is_vertical = False
                    continue
                multiply_scale = int(stream.next())
                print 'Multiply vector lengths by next byte ', multiply_scale
                scale /= multiply_scale

            else:
                raise Exception('cant do cmd %s', cmd)


# run()


#!/usr/bin/env python

# import math
# import cairo

# WIDTH, HEIGHT = 256, 256

# surface = cairo.ImageSurface (cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
# ctx = cairo.Context (surface)

# ctx.scale (WIDTH, HEIGHT) # Normalizing the canvas

# pat = cairo.LinearGradient (0.0, 0.0, 0.0, 1.0)
# pat.add_color_stop_rgba (1, 0.7, 0, 0, 0.5) # First stop, 50% opacity
# pat.add_color_stop_rgba (0, 0.9, 0.7, 0.2, 1) # Last stop, 100% opacity

# ctx.rectangle (0, 0, 1, 1) # Rectangle(x0, y0, x1, y1)
# ctx.set_source (pat)
# ctx.fill ()

# ctx.translate (0.1, 0.1) # Changing the current transformation matrix

# ctx.move_to (0, 0)
# # Arc(cx, cy, radius, start_angle, stop_angle)
# ctx.arc (0.2, 0.1, 0.1, -math.pi/2, 0)
# ctx.line_to (0.5, 0.1) # Line to (x,y)
# # Curve(x1, y1, x2, y2, x3, y3)
# ctx.curve_to (0.5, 0.2, 0.5, 0.4, 0.2, 0.8)
# ctx.close_path ()

# ctx.set_source_rgb (0.3, 0.2, 0.5) # Solid color
# ctx.set_line_width (0.02)
# ctx.stroke ()

# # surface.write_to_png ("example.png") # Output to PNG



# sf = cairocffi.ImageSurface.create_from_png(buffer)
import cairocffi

sf = cairocffi.ImageSurface(cairocffi.FORMAT_RGB24, 400, 400)
ctx = cairocffi.Context(sf)

ctx.set_antialias(cairocffi.ANTIALIAS_BEST) # ANTIALIAS_NONE # ANTIALIAS_BEST

# ctx.fill()

with ctx:
    ctx.set_source_rgb(1,1,1)
    ctx.paint()

with ctx:
    ctx.set_source_rgb(0,0,0,)

    ctx.set_line_width(1)

    ctx.move_to(50.5,50.5)
    # ctx.rel_move_to(100,100)
    ctx.rel_move_to(100,100)
    # ctx.rel_line_to(0 ,20)
    # ctx.rel_line_to(20 ,0)

    mx = cairocffi.Matrix(
        xx=1, yx=0,
        xy=0, yy=-1,
        x0=0, y0=0)

    ctx.set_matrix(
        mx
        )

    for it in range(1, 10):
    
        ctx.move_to(150.5,-50.5)

        ctx.rel_move_to(-100, -14.0*it)

        ctx.save()
        words = u'1234567890'

        for w in words:
            i = ord(w)
            stream = iter(paths[i])
            run(ctx, stream, it)

        

        ctx.restore()

    ctx.stroke()

# rel_line_to

fileobj = open('img.png', 'wb')
sf.write_to_png(fileobj)

fileobj.close()


import StringIO

from PIL import Image
sf.flush()

bf = StringIO.StringIO()

sf.write_to_png(bf)

bf.seek(0)

img = Image.open(bf)

# draw = ImageDraw.Draw(img)

# draw_copyright(draw, img)
# draw_scale(draw, img)
# draw_arrow_text(draw, img, text_coords)

img.show()



# import IPython; IPython.embed()