

# About

AutoCAD's SHX(SHP) font renderer prototype. 

SHP is an AutoCAD's text format for defining line shapes, which is also used for defining one-line fonts.

SHX is packed SHP. It can be decompiled by [DUMPSHX](https://knowledge.autodesk.com/support/autocad/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/AutoCAD-Core/files/GUID-CF541EED-CFBA-47EA-9A1A-CDE17D14FB20-htm.html) tool in express folder of autocad folder.

Its a Proof Of Concept so code quality is absent, most commands are not implemented and some glyphs renderers with errors.
I want to port it in JavaScript to draw on a canvas.


# Motivation

There are two main [font formats](https://wiki.archlinux.org/index.php/fonts#Font_formats): bitmap and outline.

Alongside of them there are another font type which is represented not by outline but line path. 

Glyphs are represented by path, so we are free to choose line width, joints, reduce a font rendering problem to a line/joints rendering. 

Thay called single line, stroke, or open loop fonts.

Its perfect for CNC milling, engrawing. [SolidWorks agree with me](https://www.cati.com/blog/2013/09/is-there-a-single-line-font-in-solidworks/)

[Here](http://imajeenyus.com/computer/20150110_single_line_fonts/index.shtml) is another article about it.

Despite one-line fonts have TTF version, it renders too thin on low-dpi screens because of "outline nature" of TTF. Of course we can play with font weight but anyway its not a canonical way to render this fonts.

When i started this research I was expecting this fonts would not blurry by subpixel rendering but that was mistaken. Even if we find font with 90 and 45 degree lines we couldn scale this font to not get into subpixel and get blurry.

Nothing is better than bitmap fonts for low-dpi screens. 

# Few notes about format:

AutoCAD's font format should not be confused with the Esri shape format.

Main list of codes is here - [Special Codes Reference](http://help.autodesk.com/view/ACD/2016/ENU/?guid=GUID-06832147-16BE-4A66-A6D0-3ADF98DC8228)

Hex bytes followed by 1(pen down) or 2(pen up) are main way to move pointer. First digit is length, second is direction. direction os coded by [About Vector Length and Direction Codes](http://help.autodesk.com/view/ACD/2016/ENU/?guid=GUID-0A8E12A1-F4AB-44AD-8A9B-2140E0D5FD23).

There are 8 and 9 command as well.

Sample glyph:


```
  *00022,37,kdblqt
  2,14,8,(-4,-25),8,(2,24),1,01A,016,012,01E,02C,02B,01A,2,8,(8,5),
  1,01A,016,012,01E,02C,02B,01A,2,8,(9,-19),14,8,(-11,9),0
```


# If you want to play with the code:
## Dependencies:

```
cairocffi
PIL (or pillow)
```

## Run:
```
$ python2 main.py
```

![image](sample.png)

Font file should be without an header, comments, etc(parser cant parse it)





# Some urls:
https://www.cadcorner.ca/textstyles.php
